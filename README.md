# MAQE Bot Challenge

## Implementations

- Aug, 2018 - [First implementation](https://gitlab.com/osama-php-projects/maqe/-/tree/v1)
- Nov, 2021 - [Second implementation](https://gitlab.com/osama-php-projects/maqe/-/tree/v2)

---

## Source

https://maqe.github.io/maqe-bot.html

## Challenge

![challenge](./challenge.png)
